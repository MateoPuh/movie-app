package com.example.movieapp.data.api.model

import com.squareup.moshi.Json

class ApiGenre {

    @Json(name = "id")
    val id: Int? = null

    @field:Json(name = "name")
    val name: String? = null
}