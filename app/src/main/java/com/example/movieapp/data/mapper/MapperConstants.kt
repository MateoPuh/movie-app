package com.example.movieapp.data.mapper

class MapperConstants {

    companion object {
        const val DEFAULT_STRING = ""
        const val DEFAULT_INT = 0
        const val DEFAULT_ID = -1
        const val DEFAULT_FLOAT = 0.0
        const val DEFAULT_BOOLEAN = false
    }
}