package com.example.movieapp.domain

data class Genre(
    val id: Int,
    val name: String
) 