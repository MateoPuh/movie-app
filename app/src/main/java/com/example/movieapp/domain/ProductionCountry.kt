package com.example.movieapp.domain

data class ProductionCountry(
    val isoCode: String,
    val name: String
)